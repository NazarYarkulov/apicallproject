﻿using ApiCallProject.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;

namespace ApiCallProject.Services
{
    public class CallService : ICallService
    {
        private ApplicationContext db;
        public CallService(ApplicationContext context)
        {
            db = context;
        }
        public void Send()
        {
            string url = "https://www.google.com/webhp?hl=ru&sa=X&ved=0ahUKEwjkvoj74oTtAhUNiYsKHaJDBogQPAgI";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            System.Diagnostics.Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            response.Close();

            stopWatch.Stop();

            var callModel = new CallModel
            {
                Date = DateTime.Now,
                ConectionTime = stopWatch.ElapsedMilliseconds,
                Link = url
            };

            db.Add(callModel);
            db.SaveChanges();
        }
        public List<CallModel> GetCallModels()
        {
            return db.CallModels.ToList();
        }
    }
}
