﻿using ApiCallProject.Models;
using System.Collections.Generic;

namespace ApiCallProject.Services
{
    public interface ICallService
    {
        void Send();
        public List<CallModel> GetCallModels();
    }
}
