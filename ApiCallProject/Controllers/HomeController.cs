﻿using Microsoft.AspNetCore.Mvc;
using ApiCallProject.Services;

namespace ApiCallProject.Controllers
{
    public class HomeController : Controller
    {
        private ICallService _callservice;
        public HomeController(ICallService callService)
        {
            _callservice = callService;
        }

        public IActionResult Index()
        {
            _callservice.Send();
            return View(_callservice.GetCallModels());
        }

    }
}
