﻿using Microsoft.EntityFrameworkCore;

namespace ApiCallProject.Models
{
    public class ApplicationContext : DbContext
    {
        public DbSet<CallModel> CallModels { get; set; }
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();   
        }
    }
}
