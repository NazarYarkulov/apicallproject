﻿using System;

namespace ApiCallProject.Models
{
    public class CallModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Link { get; set; }
        public double ConectionTime { get; set; }

    }
}
